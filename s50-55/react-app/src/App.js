import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

export default function App() {

  // State hook for the user state that's defined here is for a global scope.
  // To identify whether a user is already logged in or not, by means of localStorage
  // const [ user, setUser ] = useState({
  //   email: localStorage.getItem('email')
  // });
  
  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    //fetch('http://localhost:4000/users/details', {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    // A common pattern in React for the component to return multiple elements
    <>
    <UserProvider value={{ user, setUser, unsetUser }} >
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}