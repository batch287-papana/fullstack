// The "document" refers to the whole webpage
// The query selector is used to select a specific object (HTML element) from the document . (Webpage)

const txtFirstName = document.querySelector('#txt-first-name');

const spanFullName = document.querySelector('#span-full-name');

// Alternatively, we can use the getelement functions to retrieve the element
	// document.getElementById
	// document.getElementByClassName
	// document.getElementByITagName

// Whenever a user interact with a webpage this action is considered as a event
// The "addEventListener is a function that takes two arguments,
// String identifying the event(keyup)
// function that the listener will execute once the specified event is triggered(event)

txtFirstName.addEventListener('keyup', (event) => {
	// ThebinnerHTML property sets or returns HTML content
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	// The event.target contain the element where the event happned
	console.log(event.target);
	// The "event.target.value" gets the value of the input object(similar to the textFirstName.value);
	console.log(event.target.value);
});